# Description
This Dockerfile generates an image containing cpplint.

# Usage
- `docker build -t cpplint .`
- `docker run --volume "$PWD:/code" cpplint`
<br /> OR
- `docker run --volume "$PWD:/code" registry.gitlab.com/francoissevestre/dockerfiles/cpplint` 

# Requirements
- $PWD must contain the files to be analysed.
